Prípady použitia
================
## Vlož záznam
Predpoklad: Používateľ má oprávnenie na vkladanie zvoleného druhu záznamu.

1. Používateľ zvolí „Vyhľadať osobu“
2. Aktivuje sa UC Vyhľadaj osobu
3. Používateľ zvolí „Pridať záznam“
4. Používateľ si vyberie druh záznamu
5. Systém vyzve používateľa na pridanie záznamu
6. Používateľ napíše záznam a zvolí jeho uloženie
7. Systém uloží záznam

## Začierni informáciu
Predpoklad: Používateľ má oprávnenie na začierňovanie informácií.

1. Používateľ zvolí „Vyhľadať osobu“
2. Aktivuje sa UC Vyhľadaj osobu
3. Používateľ zvolí editovanie konkrétneho záznamu
5. Používateľ vyznačí časť záznamu a zvolí „Začierniť“
6. Systém začierni zvolenú časť záznamu
6. Používateľ pokračuje v začierňovaní textu – prípad použitia pokračuje krokom 5
7. Používateľ môže kedykoľvek uložiť alebo zahodiť vykonané zmeny

## Spravuj osobu (CRUD)

## Vyhľadaj prepojené osoby
1. Používateľ zvolí „Vyhľadať osobu“
2. Aktivuje sa UC Vyhľadaj osobu
3. Používateľ zvolí „Vyhľadať prepojené osoby“
4. Systém umožní používateľovi zadať kritériá vyhľadávania podľa typu prepojenia (kolegovia, človek v prípade). Taktiež podľa hĺbky prepojenia (osoby sa poznajú priamo alebo cez ďalšie osoby).
5. Systém zobrazí zoznam osôb, ktoré zodpovedajú kritériám.

## Zaraď policajta do oddelenia
Predpoklad: Používateľ má oprávnenie na zaradenie policajta do oddelenia

1. Používateľ zvolí „Vyhľadať osobu“
2. Aktivuje sa UC Vyhľadaj osobu
3. Používateľ zvolí „Presuň do oddelenia“
2. Aktivuje sa UC Vyhľadaj oddelenie
3. Systém uloží zaradenie policajta do oddelenia

## Priraď policajtovi prípad
Predpoklad: Používateľ má oprávnenie priraďovať policajtom prípad.

1. Používateľ zvolí „Vyhľadať prípad“
2. Aktivuje sa UC Vyhľadaj prípad
1. Používateľ zvolí „Priradiť prípad“
2. Aktivuje sa UC Vyhľadaj osobu
5. Systém uloží priradenie prípadu

## Vyhľadať osobu
Predpoklad: Používateľ zvolil „Vyhľadať osobu“.

1. Systém ponúkne používateľovi na výber naposledy navštívené osoby
2. Systém umožní používateľovi zadať kritériá vyhľadávania podľa podľa mena, postavenia, oddelenia, veku alebo čísla OP.
3. Používateľ zadá kritéria vyhľadávania a potvrdí.
4. Systém zobrazí zoznam osôb, ktoré zodpovedajú kritériám.
5. Používateľ vyberie konkrétnu osobu zo zoznamu.

## Vyhľadaj policajta bez prípadu
1. Používateľ zvolí „Vyhľadať policajta bez prípadu“
2. Systém umožní používateľovi zadať kritériá vyhľadávania podľa podľa mena, postavenia, oddelenia, veku alebo čísla OP.
3. Používateľ zadá kritéria vyhľadávania a potvrdí.
4. Systém zobrazí zoznam osôb, ktoré zodpovedajú kritériám.
5. Používateľ vyberie konkrétnu osobu zo zoznamu.